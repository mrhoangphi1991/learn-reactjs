import React from 'react'
import Loadable from 'react-loadable';
import Loading from './Loading';
import {
    Route
} from 'react-router-dom'

const Router = (props) => {
    let routeMaps = props.routes.map(route => {
        return {
            path: route.path,
            component: Loadable({
                loader: () => {
                    return import(`./components/${route.component}`); // must use value not ref
                },
                loading: Loading,
            })
        };
    })

    return (
        <div>
            {routeMaps.map(val => {
                return <Route key={val.path} path={val.path} component={val.component} />
            })}
        </div>
    );
}
export default Router;