import React from "react";
import BaseComponent from "../base/base.component";
import Axios from "axios";
// services import

export default class Form extends BaseComponent {
  state = { userName: "" };
  handleSubmit = event => {
    event.preventDefault();
    Axios.get(`https://api.github.com/users/${this.state.userName}`).then(
      res => {
        console.log(res);
        this.props.onSubmit(res.data);
        this.setState({ userName: "" }); // reset input
      }
    );
  };

  render = () => {
    return (
      <form onSubmit={this.handleSubmit}>
        {this.props.children}
        <input
          // ref={input => {
          //   this.userNameInput = input;
          // }}
          value={this.state.userName}
          onChange={event => {
            this.setState({ userName: event.target.value });
          }}
          type="text"
          placeholder="github username"
        />
        <button type="submit">Add Card</button>
      </form>
    );
  };
}
