import React from "react";
import ReactDOM from "react-dom";

// styles
import "./index.scss";
import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";

// scripts

import * as _ from "lodash";
// components
import App from "./app";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
