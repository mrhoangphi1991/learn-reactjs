import React from "react";
import { Link } from "react-router-dom";

const Menus = (props) => {
    const menus = props.routes.map(x => {
        return <li key={x.path}><Link to={x.path}>{x.name}</Link></li>
    })
    return (
        <div>
            <ul>
                {menus}
            </ul>
        </div>
    );
}
export default Menus;