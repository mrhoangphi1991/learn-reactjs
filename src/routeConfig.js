const ROUTES = [
    { path: '/game9', component: 'play9/play9.component', name: "game9" },
    { path: '/card', component: 'card/card.component',  name: "card" },
    { path: '/home', component: 'home/home.component',  name: "home" },
];
export default ROUTES;