import React from 'react'
import {
  BrowserRouter,
} from 'react-router-dom'
import Router from "./router"
import ROUTES from "./routeConfig"
import Menus from './menus';

const App = () => (
  <BrowserRouter>
    {/* must wrap it inside an element */}
    <div>
      <Menus routes={ROUTES}></Menus>
      <Router routes={ROUTES}></Router>
    </div>
  </BrowserRouter>
)
export default App