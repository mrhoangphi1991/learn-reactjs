import React from "react";
import "./stars.component.scss";

export const Stars = props => {
  let stars = [];
  for (let i = 0; i < props.numberOfStar; i++) {
    stars.push(<i key={i} className="fa fa-star" />);
  }
  return <div className="star col-md-5">{stars}</div>;
};
