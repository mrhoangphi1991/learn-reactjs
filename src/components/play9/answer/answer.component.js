import React, { Component } from "react";
// import "./stars.component.scss";

export class Answer extends Component {
  render = () => {
    console.log(this, "render");
    return (
      <div className="answer col-md-5">
        {this.props.selectedNumbers.map((value, index) => (
          <span key={index} onClick={() => this.props.unselectNumber(value)}>
            {value}
          </span>
        ))}
      </div>
    );
  };

  componentDidMount = () => {
    console.log(this, "component did mount");
  };
  componentWillMount = () => {
    console.log(this, "component will mount");
  };
  componentWillUpdate = () => {
    console.log(this, "component will update");
  };
  componentDidUpdate = () => {
    console.log(this, "component did update");
  };
  componentWillReceiveProps = () => {
    console.log(this, "component will receive props");
  };
  componentDidCatch = error => {
    console.log(error);
  };
}
