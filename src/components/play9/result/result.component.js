import React from "react";
import "./result.component.scss";
export const Result = props => {
  return <div className="result text-center col-md-12">{props.message}</div>;
};
