import React from "react";

import BaseComponent from "../../core/base/base.component";

import "./play9.component.scss";

import { Stars } from "./star/stars.component";
import { Button } from "./button/button.component";
import { Answer } from "./answer/answer.component";
import { Numbers } from "./number/number.component";
import { Result } from "./result/result.component";
export default class GamePlay9 extends BaseComponent {
  state = {
    selectedNumbers: [],
    numberOfStar: GamePlay9.randomNumberOfStar(),
    answerIsCorrect: null,
    usedNumbers: [],
    redraws: 10,
    doneStatus: null
  };
  static randomNumberOfStar = () => 1 + Math.round(Math.random() * 8);

  possibleSolution = ({ usedNumbers, numberOfStar }) => {
    const possibleNumbers = Array.from(new Array(9), (n, i) => i + 1).filter(
      val => usedNumbers.indexOf(val) === -1
    );
    return this.sumOfNumbers(possibleNumbers, 0);
  };

  sumOfNumbers(possibleNumbers, sum) {
    const stopNumber = this.state.numberOfStar - sum;
    if (stopNumber === 0) {
      return true;
    }
    if (possibleNumbers.length === 0) {
      return false;
    }

    let possibleNumbers2 = possibleNumbers
      .slice(1)
      .filter(x => x <= stopNumber);
    for (let i = 0; i < possibleNumbers.length; i++) {
      if (this.sumOfNumbers(possibleNumbers2, sum + possibleNumbers[i])) {
        return true;
      }
    }

    return false;
  }
  updateDoneStatus = () => {
    this.setState(prev => {
      if (prev.usedNumbers.length === 9) {
        return { doneStatus: "Done. Nice!" };
      }
      if (prev.redraws === 0 && !this.possibleSolution(prev)) {
        return { doneStatus: "Game Over!" };
      }
    });
  };

  checkAnswer = () => {
    this.setState(prev => ({
      answerIsCorrect:
        prev.selectedNumbers.length > 0
          ? prev.selectedNumbers.reduce((pre, cur) => pre + cur) ===
          prev.numberOfStar
          : null
    }));
  };

  acceptAnswer = () => {
    this.setState(
      prev => ({
        usedNumbers: prev.usedNumbers.concat(prev.selectedNumbers),
        selectedNumbers: [],
        answerIsCorrect: null,
        numberOfStar: GamePlay9.randomNumberOfStar()
      }),
      this.updateDoneStatus
    );
  };
  selectNumber = number => {
    if (
      this.state.selectedNumbers.indexOf(number) > -1 ||
      this.state.usedNumbers.indexOf(number) > -1
    )
      return;

    this.setState(prev => ({
      selectedNumbers: prev.selectedNumbers.concat(number),
      answerIsCorrect: null
    }));
  };
  unselectNumber = number => {
    this.setState(prev => ({
      selectedNumbers: prev.selectedNumbers.filter(x => x !== number),
      answerIsCorrect: null
    }));
  };

  refresh = () => {
    this.setState(prev => {
      if (prev.redraws === 0) return;
      return {
        redraws: prev.redraws - 1,
        numberOfStar: GamePlay9.randomNumberOfStar(),
        selectedNumbers: [],
        answerIsCorrect: null
      };
    }, this.updateDoneStatus());
  };
  render() {
    const {
      numberOfStar,
      selectedNumbers,
      answerIsCorrect,
      usedNumbers,
      redraws,
      doneStatus
    } = this.state;
    console.log(this, "render");
    return (
      <div className="play9">
        <h3>play nine</h3>
        <hr />
        {!this.state.doneStatus ? (
          <div>
            <div className="row">
              <Stars numberOfStar={numberOfStar} />
              <Button
                refresh={this.refresh}
                refreshNumbers={redraws}
                acceptAnswer={this.acceptAnswer}
                checkAnswer={this.checkAnswer}
                answerIsCorrect={answerIsCorrect}
              />
              <Answer
                unselectNumber={this.unselectNumber}
                selectedNumbers={selectedNumbers}
              />
            </div>
            <div className="row">
              <Numbers
                selectNumber={this.selectNumber}
                selectedNumbers={selectedNumbers}
                usedNumbers={usedNumbers}
              />
            </div>
          </div>
        ) : (
            <Result message={doneStatus} />
          )}
      </div>
    );
  }
  componentDidMount = () => {
    console.log(this, "component did mount");
  };
  componentWillMount() {
    console.log(this, "component will mount");
  }
  componentWillUpdate() {
    console.log(this, "component will update");
  }
  componentDidUpdate() {
    console.log(this, "component did update");
  }
  componentWillReceiveProps() {
    console.log(this, "component will receive props");
  }
  componentDidCatch = error => {
    console.log(error);
  };
}
