import React, { Component } from "react";
import "./button.component.scss";

export class Button extends Component {
  render() {
    console.log(this, "render");
    let btnAnswer,
      btnRefresh = (
        <button
          className={`btn btn-danger ${
            this.props.refreshNumbers === 0 ? "disabled" : ""
          }`}
          onClick={this.props.refresh}
        >
          <i className="fa fa-refresh" />
          {this.props.refreshNumbers}
        </button>
      );
    switch (this.props.answerIsCorrect) {
      case true:
        btnAnswer = (
          <button className="btn btn-success" onClick={this.props.acceptAnswer}>
            <i className="fa fa-check" />
          </button>
        );
        break;
      case false:
        btnAnswer = (
          <button className="btn btn-danger">
            <i className="fa fa-times" />
          </button>
        );
        break;

      default:
        btnAnswer = (
          <button className={`btn`} onClick={this.props.checkAnswer}>
            =
          </button>
        );
        break;
    }
    return (
      <div className="col-md-2">
        {btnAnswer}
        <br />
        <br />
        {btnRefresh}
      </div>
    );
  }
  componentDidMount = () => {
    console.log(this, "component did mount");
  };
  componentWillMount() {
    console.log(this, "component will mount");
  }
  componentWillUpdate() {
    console.log(this, "component will update");
  }
  componentDidUpdate() {
    console.log(this, "component did update");
  }
  componentWillReceiveProps() {
    console.log(this, "component will receive props");
  }
  componentDidCatch = error => {
    console.log(error);
  };
}
