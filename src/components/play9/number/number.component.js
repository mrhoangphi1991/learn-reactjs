import React from "react";
import "./number.component.scss";
export const Numbers = props => {
  var numbers = Array.from(new Array(9), (n, i) => i + 1);

  const getClassName = number => {
    if (props.usedNumbers.indexOf(number) > -1) {
      return "used";
    }
    if (props.selectedNumbers.indexOf(number) > -1) {
      return "selected";
    }
  };
  return (
    <div className="number text-center col-md-12">
      {numbers.map((value, index) => (
        <span
          key={index}
          className={getClassName(value)}
          onClick={() => props.selectNumber(value)}
        >
          {value}
        </span>
      ))}
    </div>
  );
};
