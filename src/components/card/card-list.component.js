import React from "react";
import { CardItem } from "./card-item.component";

export const CardList = props => {
  return (
    // <React.Fragment>
    //   {/* return multible  childrent without wrapping nodes*/}
    // </React.Fragment>
    <div className="card-list">
      {props.cards.map(card => (
        <CardItem
          key={card.id}
          card={card}
          removeCard={props.removeCard(card.id)}
        />
      ))}
    </div>
  );
};

export default CardList;
