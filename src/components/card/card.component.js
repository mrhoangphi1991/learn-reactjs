// import core lib
import React from "react";
import Axios from "axios";
// import components
import BaseComponent from "../../core/base/base.component";
import Form from "../../core/form/form.component";
import CardList from "./card-list.component";
// styles
import "./card.component.scss";

export default class Card extends BaseComponent {
  state = {
    cards: [
      {
        id: 1,
        avatar_url: "https://avatars0.githubusercontent.com/u/1?v=4",
        name: "Phi nguyen",
        company: "company name"
      },
      {
        id: 2,
        avatar_url: "https://avatars0.githubusercontent.com/u/1?v=4",
        name: "Phi nguyen1",
        company: "company name"
      },
      {
        id: 3,
        avatar_url: "https://avatars0.githubusercontent.com/u/1?v=4",
        name: "Phi nguyen1",
        company: "company name"
      },
      {
        id: 4,
        avatar_url: "https://avatars0.githubusercontent.com/u/1?v=4",
        name: "Phi nguyen1",
        company: "company name"
      },
      {
        id: 5,
        avatar_url: "https://avatars0.githubusercontent.com/u/1?v=4",
        name: "Phi nguyen1",
        company: "company name"
      },
      {
        id: 6,
        avatar_url: "https://avatars0.githubusercontent.com/u/1?v=4",
        name: "Phi nguyen1",
        company: "company name"
      }
    ]
  };
  getCardList() {
    Axios.get("https://api.github.com/users").then(res => {
      this.setState({
        cards: res.data
      });
    });
  }
  addCard = card => {
    this.setState(prevState => ({
      cards: prevState.cards.concat(card)
    }));
  };
  removeCard = cardId => {
    this.setState(prev => {
      cards: prev.cards.filter(item => item.id !== cardId);
    });
  };
  render() {
    return (
      <div className="card-box">
        <Form onSubmit={this.addCard}>
          <h1>Phi Nguyen</h1>
        </Form>
        <CardList cards={this.state.cards} removeCard={this.removeCard} />
        <div className="clear-fix"></div>
      </div>
    );
  }
}
