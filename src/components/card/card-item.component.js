import BaseComponent from "../../core/base/base.component";
import React from "react";

export class CardItem extends BaseComponent {
  removeCard = id => {
    this.props.removeCard(id);
  };
  render() {
    return (
      <div className="card-item">
        <div className="card-item__header">
          <span className="btn btn--close" />
          <img
            className="card__image"
            src={this.props.card.avatar_url}
            alt="avatar"
          />
        </div>
        <div className="avatar__box">
          <div className="avatar__name">
            Login name : {this.props.card.login}
          </div>
          <div className="company__name">Role : {this.props.card.type}</div>
        </div>
      </div>
    );
  }
}
