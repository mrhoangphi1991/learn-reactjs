// import core lib
import React from "react";
import Axios from "axios";
// import components
import BaseComponent from "../../core/base/base.component";
// styles
import "./clock.component.scss";

export class Clock extends BaseComponent {
  state = {};

  render() {
    return (
      <div className="clock">
        <div className="clock-box">
          <div className="clock__hour">hour</div>
          <div className="clock__min">min</div>
          <div className="clock__second">second</div>
        </div>
        <div className="numbers">
          <span>12</span>
          <span>3</span>
          <span>6</span>
          <span>9</span>
        </div>
      </div>
    );
  }
}
