import Axios from "axios";

export default class BaseService {
  baseUrl = '';
  constructor(http) {
    this.http = http || Axios;
  }
  get(url) {
    return this.http.get(`${this.baseUrl}${url}`);
  }
  push(url, data) {
    return this.http.post(`${this.baseUrl}${url}`, data);
  }
  patch(url, data) {
    return this.http.patch(`${this.baseUrl}${url}`, data);
  }
  delete(url, data) {
    return this.http.post(`${this.baseUrl}${url}`, data);
  }
}
